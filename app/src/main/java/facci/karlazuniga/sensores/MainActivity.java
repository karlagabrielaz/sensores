package facci.karlazuniga.sensores;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnsensores = findViewById(R.id.btnSensores);
        //Button btnaproximacion = findViewById(R.id.btnAproximacion);
        Button btnluz = findViewById(R.id.btnLuz);
        Button btngps = findViewById(R.id.btnGPS);

        btnsensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),ActividadSensorAcelerometro.class);
                startActivity(intent);
            }
        });

        /*btnaproximacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(),SensorAproximacion.class);
                startActivity(intent);
            }
        });*/

        btnluz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent luz = new Intent(v.getContext(),SensorLuz.class);
                startActivity(luz);
            }
        });

        btngps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent gps = new Intent(v.getContext(),SensorGps.class);
                startActivity(gps);
            }
        });

    }


}

